## Setup
1. Added files to Unreal Engine project inside of the content folder.
2. Create the widget named "NPuzzle" (Make sure you add to the viewport, show the cursor, and set UIModeOnly).
3. In the variables section, choose the difficulty, tile sounds, and randomization that you desire.
4. Play!

`Note:` If you'd like to do something after the puzzle has completed, just call the event dispatcher.


## Current Version
`Unreal Engine 4.19`